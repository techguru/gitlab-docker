#
# Cookbook Name:: gitlab-docker
# Recipe:: default
#
# Copyright 2016, GitLab Inc.
#
# All rights reserved - Do Not Redistribute
#
include_recipe 'gitlab-vault'

## Fetch secrets from Chef Vault
docker_conf = GitLab::Vault.get(node, 'gitlab-docker')

## docker
docker_service 'default' do
  action [:create, :start]
end

docker_conf.each do |image, v|
  next if image == 'chef_vault'

  docker_image image do
    action :pull
    tag v['tag'] if v['tag']
  end

  container_name = image.tr('/:', '_')
  docker_container container_name do
    repo image
    tag v['tag'] if v['tag']
    env v['env'] if v['env']
    command v['command'] if v['command']
    volumes v['volumes'] if v['volumes']
    port v['docker_port']
  end
end

## nginx
include_recipe 'chef_nginx'
template "#{node['nginx']['dir']}/sites-available/docker" do
  source 'nginx-site-docker.erb'
  owner  'root'
  group  'root'
  mode   '0644'
  variables :docker_conf => docker_conf
  notifies :restart, resources(:service => 'nginx'), :delayed
end

nginx_site 'docker' do
  enable true
end

docker_conf.each do |image, v|
  next if image == 'chef_vault'

  file "/etc/ssl/#{v['virtualhost']}.crt" do
    content v['ssl_certificate']
    owner 'root'
    group 'root'
    mode 0644
    notifies :restart, resources(:service => 'nginx'), :delayed
  end

  file "/etc/ssl/#{v['virtualhost']}.key" do
    content v['ssl_key']
    owner 'root'
    group 'root'
    mode 0600
    notifies :restart, resources(:service => 'nginx'), :delayed
  end
end
