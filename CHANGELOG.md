gitlab-docker CHANGELOG
=======================

This file is used to list changes made in each version of the gitlab-docker cookbook.

0.1.1
-----
- Jeroen Nijhof - Use gitlab-vault and chef_nginx

0.1.0
-----
- Jeroen Nijhof - Initial release of gitlab-docker

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
