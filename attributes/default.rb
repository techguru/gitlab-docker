default['gitlab-docker'] = {}
default['nginx']['default_site_enabled'] = false

# Example
#default['gitlab-docker']['minio/minio'] = {}
#default['gitlab-docker']['minio/minio']['virtualhost'] = 'example.com'
#default['gitlab-docker']['minio/minio']['port'] = '443'
#default['gitlab-docker']['minio/minio']['docker_port'] = '9000'
#default['gitlab-docker']['minio/minio']['ssl_certificate'] = 'override_attribute in vault!'
#default['gitlab-docker']['minio/minio']['ssl_key'] = 'override_attribute in vault!'

